digiKam Snap Repository
-----------------------

Bundle is processed when new release is published only.

- Front page:      https://snapcraft.io/digikam
- Documentation:   https://community.kde.org/Guidelines_and_HOWTOs/Snap
