/* ============================================================
 *
 * This file is a part of digiKam project
 * https://www.digikam.org
 *
 * Date        : 2024-11-10
 * Description : Performs face detection and recognition
 *
 * SPDX-FileCopyrightText: 2024-2025 by Gilles Caulier <caulier dot gilles at gmail dot com>
 * SPDX-FileCopyrightText: 2024-2025 by Michael Miller <michael underscore miller at msn dot com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * ============================================================ */

#include "facepipelinedetectrecognize.h"

// Qt includes

#include <QList>
#include <QSet>
#include <QElapsedTimer>
#include <QRectF>

// KDE includes

#include <klocalizedstring.h>

// Local includes

#include "digikam_debug.h"
#include "digikam_opencv.h"
#include "sharedqueue.h"
#include "collectionmanager.h"
#include "album.h"
#include "iteminfo.h"
#include "coredb.h"
#include "facescansettings.h"
#include "dimg.h"
#include "previewloadthread.h"
#include "faceutils.h"
#include "facepipelinepackagebase.h"
#include "thumbnailloadthread.h"
#include "faceclassifier.h"
#include "identityprovider.h"
#include "qtopencvimg.h"

namespace Digikam
{

FacePipelineDetectRecognize::FacePipelineDetectRecognize(const FaceScanSettings& _settings)
    : FacePipelineBase(_settings)
{
}

FacePipelineDetectRecognize::~FacePipelineDetectRecognize()
{
    if (faceDetector)
    {
        delete faceDetector;
    }

    // extractor is singleton, so no need to delete it
}

bool FacePipelineDetectRecognize::start()
{

    // create the face detector and extractor

    try
    {
        faceDetector  = new DNNFaceDetectorYuNet;
        faceExtractor = static_cast<DNNModelSFace*>(DNNModelManager::instance()->getModel(QStringLiteral("sface"),
                                                                                          DNNModelUsage::DNNUsageFaceRecognition));
        faceExtractor->getNet();
    }

    catch (const std::exception& e)
    {
        std::cerr << e.what() << '\n';

        if (faceDetector)
        {
            delete faceDetector;
            faceDetector = nullptr;
        }

        // extractor is singleton, so no need to delete it
    }

    catch (...)
    {
        if (faceDetector)
        {
            delete faceDetector;
            faceDetector = nullptr;
        }
    }

    // check if the detector and extractor were created

    if ((nullptr == faceDetector) || (nullptr == faceExtractor) || !faceExtractor->modelLoaded)
    {
        return false;
    }

    // set the face detection size and accuracy

    faceDetector->setFaceDetectionSize(settings.detectSize);
    faceDetector->uiConfidenceThreshold = settings.detectAccuracy;

    {
        // use the mutex to synchronize the start of the threads

        QMutexLocker lock(&mutex);

        // add the worker threads for this pipeline

        addWorker(MLPipelineStage::Finder);
        addWorker(MLPipelineStage::Loader);
        addWorker(MLPipelineStage::Extractor);
        addWorker(MLPipelineStage::Classifier);
        addWorker(MLPipelineStage::Writer);
    }

    return FacePipelineBase::start();
}

bool FacePipelineDetectRecognize::finder()
{
    MLPIPELINE_FINDER_START(MLPipelineStage::Loader);

    /* =========================================================================================
     * Pipeline finder specific initialization code
     *
     * Use the block from here to MLPIPELINE_FINDER_END to find the IDs images to process.
     * The code in this block is run once per stage initialization. The number of instances
     * is always 1.
     */

    // get the IDs to process

    QSet<qlonglong> filter;

    for (const Album* const album : std::as_const(settings.albums))
    {
        if (cancelled)
        {
            break;
        }

        if (!album->isTrashAlbum())
        {
            // get the image IDs for the album

            QList<qlonglong> imageIds = CoreDbAccess().db()->getImageIds(album->id(),
                                                                         DatabaseItem::Status::Visible,
                                                                         (FaceScanSettings::AlreadyScannedHandling::Skip != settings.alreadyScannedHandling));

            // quick check if we should add threads.

            if (!moreCpu)
            {
                moreCpu = checkMoreWorkers(totalItemCount, imageIds.size(), settings.useFullCpu);
            }

            // iterate over the image IDs and add unique IDs to the queue for processing

            for (qlonglong imageId : std::as_const(imageIds))
            {
                // filter out duplicate image IDs

                if (!filter.contains(imageId))
                {
                    ++totalItemCount;
                    filter << imageId;
                    enqueue(nextQueue, new FacePipelinePackageBase(imageId));
                }
            }
        }
    }

    for (const ItemInfo& info : std::as_const(settings.infos))
    {
        // filter out duplicate image IDs

        qlonglong imageId = info.id();

        if (!filter.contains(imageId))
        {
            ++totalItemCount;
            filter << imageId;
            enqueue(nextQueue, new FacePipelinePackageBase(imageId));
        }
    }

    /* =========================================================================================
     * Pipeline finder specific cleanup
     *
     * Use the block from here to MLPIPELINE_FINDER_END to clean up any resources used by the stage.
     */

    MLPIPELINE_FINDER_END(MLPipelineStage::Loader);
}

bool FacePipelineDetectRecognize::loader()
{
    MLPIPELINE_STAGE_START(QThread::LowPriority, MLPipelineStage::Loader, MLPipelineStage::Extractor);
    FacePipelinePackageBase* package = nullptr;

    /* =========================================================================================
     * Pipeline stage specific initialization code
     *
     * Use the block from here to MLPIPELINE_LOOP_START to initialize the stage.
     * The code in this block is run once per stage initialization. The number of instances
     * is at least 1. More instances are created by addMoreWorkers if needed.
     */

    MLPIPELINE_LOOP_START(MLPipelineStage::Loader, thisQueue);
    package = static_cast<FacePipelinePackageBase*>(mlpackage);

    /* =========================================================================================
     * Start pipeline stage specific loop
     *
     * All code from here to MLPIPELINE_LOOP_END is in a try/catch block and loop.
     * This loop is run once per image.
     */
    {
        // check if the ID is for an image (not video or other file type)

        bool sendNotification = true;

        if (DatabaseItem::Category::Image == package->info.category())
        {
            // load high quality image for detection

            package->image = PreviewLoadThread::loadHighQualitySynchronously(package->info.filePath());

            // check for corrupted images that can't be loaded

            if (!package->image.isNull())
            {
                // create a thumbnail for the notification

                package->thumbnailIcon = QIcon(package->image.smoothScale(48, 48, Qt::KeepAspectRatio).convertToPixmap());

                // send to the next stage

                enqueue(nextQueue, package);

                sendNotification = false;
            }
        }

        if (sendNotification)
        {
            // send a notification that the file was skipped

            notify(MLPipelineNotification::notifySkipped,
                   package->info.name(),
                   package->info.relativePath(),
                   QString(),
                   0,
                   package->thumbnailIcon);

            // delete the package since it is not needed

            delete package;
        }
    }

    /* =========================================================================================
     * End pipeline stage specific loop
     */

    MLPIPELINE_LOOP_END(MLPipelineStage::Loader, "FacePipelineDetectRecognize::loader");

    /* =========================================================================================
     * Pipeline stage specific cleanup
     *
     * Use the block from here to MLPIPELINE_STAGE_END to clean up any resources used by the stage.
     */

    MLPIPELINE_STAGE_END(MLPipelineStage::Loader, MLPipelineStage::Extractor);
}

bool FacePipelineDetectRecognize::extractor()
{
    MLPIPELINE_STAGE_START(QThread::NormalPriority, MLPipelineStage::Extractor, MLPipelineStage::Classifier);
    FacePipelinePackageBase* package = nullptr;

    /* =========================================================================================
     * Pipeline stage specific initialization code
     *
     * Use the block from here to MLPIPELINE_LOOP_START to initialize the stage.
     * The code in this block is run once per stage initialization. The number of instances
     * is at least 1. More instances are created by addMoreWorkers if needed.
     */

    FaceUtils utils;

    MLPIPELINE_LOOP_START(MLPipelineStage::Extractor, thisQueue);
    package = static_cast<FacePipelinePackageBase*>(mlpackage);

    /* =========================================================================================
     * Start pipeline stage specific loop
     *
     * All code from here to MLPIPELINE_LOOP_END is in a try/catch block and loop.
     * This loop is run once per image.
     */

    {
        // preprocess the image

        // copy the image to a cv::UMat

        cv::UMat cvUImage       = QtOpenCVImg::image2Mat(
                                                         package->image,
                                                         CV_8UC3,
                                                         QtOpenCVImg::MatColorOrder::MCO_RGB
                                                        )
                                                        .getUMat(cv::ACCESS_RW);

        // resize the image if needed. Only resize if the image is larger than the input size of the detector

        cv::Size inputImageSize = faceDetector->nnInputSizeRequired();

        if (std::max(cvUImage.cols, cvUImage.rows) > std::max(inputImageSize.width, inputImageSize.height))
        {
            // Image should be resized. YuNet image sizes are much more flexible than SSD and YOLO
            // so we just need to make sure no one bound exceeds the max. No padding needed.

            float resizeFactor      = std::min(static_cast<float>(inputImageSize.width)  / static_cast<float>(cvUImage.cols),
                                               static_cast<float>(inputImageSize.height) / static_cast<float>(cvUImage.rows));

            int newWidth            = (int)(resizeFactor * cvUImage.cols);
            int newHeight           = (int)(resizeFactor * cvUImage.rows);
            cv::resize(cvUImage, cvUImage, cv::Size(newWidth, newHeight));
        }

        // detect any faces in the image

        cv::UMat udetectionResults  = faceDetector->callModel(cvUImage);

        // process detected faces

        if (udetectionResults.rows > 0)
        {
            cv::Mat detectionResults   = udetectionResults.getMat(cv::ACCESS_READ);

            // get list of previously confirmed faces

            QList<FaceTagsIface> faces = utils.confirmedFaceTagsIfaces(package->info.id());

            // get list of previously ignored faces

            faces                     += utils.ignoredFaceTagsIfaces(package->info.id());

            QList<QRectF> faceFRects;

            // Loop through the faces found.

            for (int i = 0 ; i < detectionResults.rows ; ++i)
            {
                // Create the rect of the face.

                int X       = static_cast<int>(detectionResults.at<float>(i, 0));
                int Y       = static_cast<int>(detectionResults.at<float>(i, 1));
                int width   = static_cast<int>(detectionResults.at<float>(i, 2));
                int height  = static_cast<int>(detectionResults.at<float>(i, 3));

                // Add the rect to result list.

                faceFRects << QRectF(qreal(X)      / qreal(cvUImage.cols),
                                     qreal(Y)      / qreal(cvUImage.rows),
                                     qreal(width)  / qreal(cvUImage.cols),
                                     qreal(height) / qreal(cvUImage.rows));

                // check if rect is already assigned to a face to filter out confirmed and ignored faces

                bool found = false;

                if (faces.size() > 0)
                {
                    for (int j = 0; j < faces.size(); ++j)
                    {
                        // compute current image relative rect

                        QRect rect = QRect(package->image.width()  * faceFRects[i].x(),
                                           package->image.height() * faceFRects[i].y(),
                                           package->image.width()  * faceFRects[i].width(),
                                           package->image.height() * faceFRects[i].height());

                        if (faces[j].region().intersects(TagRegion(rect), 0.85))
                        {
                            found = true;

                            break;
                        }
                    }
                }

                // if face is not found (confirmed), then extract for classification

                if (!found)
                {
                    cv::UMat ualignedFace;
                    cv::UMat uface_features;
                    cv::Mat  face_features;

                    // extract the face vectors (features) for classification

                    {
                        QMutexLocker lock(&(faceExtractor->mutex));

                        faceExtractor->getNet()->alignCrop(cvUImage, udetectionResults.row(i), ualignedFace);

                        faceExtractor->getNet()->feature(ualignedFace, uface_features);

                        face_features = uface_features.getMat(cv::ACCESS_READ);
                    }

                    // normalize the face features if we have any

                    if (0 < face_features.rows)
                    {
                        // normalize the face features

                        cv::Mat normalized_features;
                        normalize(face_features, normalized_features);

                        // add the face features and face rect to the package

                        package->featuresList << normalized_features;
                        package->faceRects << faceFRects[i];
                    }
                }
            }
        }

        // send the package to the next stage

        enqueue(nextQueue, package);
    }

    /* =========================================================================================
     * End pipeline stage specific loop
     */

    MLPIPELINE_LOOP_END(MLPipelineStage::Extractor, "FacePipelineDetectRecognize::extractor");

    /* =========================================================================================
     * Pipeline stage specific cleanup
     *
     * Use the block from here to MLPIPELINE_STAGE_END to clean up any resources used by the stage.
     */

    MLPIPELINE_STAGE_END(MLPipelineStage::Extractor, MLPipelineStage::Classifier);
}

bool FacePipelineDetectRecognize::classifier()
{
    MLPIPELINE_STAGE_START(QThread::LowPriority, MLPipelineStage::Classifier, MLPipelineStage::Writer);
    FacePipelinePackageBase* package = nullptr;

    /* =========================================================================================
     * Pipeline stage specific initialization code
     *
     * Use the block from here to MLPIPELINE_LOOP_START to initialize the stage.
     * The code in this block is run once per stage initialization. The number of instances
     * is at least 1. More instances are created by addMoreWorkers if needed.
     */

    FaceClassifier* const classifier = FaceClassifier::instance();
    classifier->setParameters(settings);

    MLPIPELINE_LOOP_START(MLPipelineStage::Classifier, thisQueue);
    package = static_cast<FacePipelinePackageBase*>(mlpackage);

    /* =========================================================================================
     * Start pipeline stage specific loop
     *
     * All code from here to MLPIPELINE_LOOP_END is in a try/catch block and loop.
     * This loop is run once per image.
     */

    {
        for (int i = 0 ; i < package->featuresList.size() ; ++i)
        {
            // verify the feature mat is not empty

            if (0 < package->featuresList[i].rows)
            {
                // classify the features

                package->labelList << classifier->predict(package->featuresList[i]);
            }
            else
            {
                package->labelList << -1;
            }
        }

        enqueue(nextQueue, package);
    }

    /* =========================================================================================
     * End pipeline stage specific loop
     */

    MLPIPELINE_LOOP_END(MLPipelineStage::Classifier, "FacePipelineDetectRecognize::classifier");

    /* =========================================================================================
     * Pipeline stage specific cleanup
     *
     * Use the block from here to MLPIPELINE_STAGE_END to clean up any resources used by the stage.
     */

    MLPIPELINE_STAGE_END(MLPipelineStage::Classifier, MLPipelineStage::Writer);
}

bool FacePipelineDetectRecognize::writer()
{
    MLPIPELINE_STAGE_START(QThread::LowPriority, MLPipelineStage::Writer, MLPipelineStage::None);
    FacePipelinePackageBase* package = nullptr;

    /* =========================================================================================
     * Pipeline stage specific initialization code
     *
     * Use the block from here to MLPIPELINE_LOOP_START to initialize the stage.
     * The code in this block is run once per stage initialization. The number of instances
     * is at least 1. More instances are created by addMoreWorkers if needed.
     */

    IdentityProvider* const    idProvider          = IdentityProvider::instance();
    FaceUtils                  utils;
    ThumbnailLoadThread* const thumbnailLoadThread = ThumbnailLoadThread::defaultThread();
    const QList<AlbumRootInfo> roots               = CoreDbAccess().db()->getAlbumRoots();
    Q_UNUSED(roots);

    MLPIPELINE_LOOP_START(MLPipelineStage::Writer, thisQueue);
    package                                        = static_cast<FacePipelinePackageBase*>(mlpackage);

    /* =========================================================================================
     * Start pipeline stage specific loop
     *
     * All code from here to MLPIPELINE_LOOP_END is in a try/catch block and loop.
     * This loop is run once per image.
     */

    {
        switch (settings.alreadyScannedHandling)
        {
            case FaceScanSettings::Rescan:
            {
                // remove old unconfirmed face rects

                QList<FaceTagsIface> oldEntries = utils.unconfirmedFaceTagsIfaces(package->info.id());
                utils.removeFaces(oldEntries);
                break;
            }

            case FaceScanSettings::ClearAll:
            {
                // remove all face rects

                utils.removeAllFaces(package->info.id());
                break;
            }

            case FaceScanSettings::RecognizeOnly:
            case FaceScanSettings::Skip:
            {
                // do nothing
                // Skipped images were skipped in the finder stage
                // RecognizeOnly defines the pipeline

                break;
            }
        }

        // mark the image as scanned

        utils.markAsScanned(package->info);

        // create thumbnails and write the new face rects to the database

        QStringList names;

        if (package->faceRects.size())
        {
            QList<FaceTagsIface>    databaseFaces;
            QList<Identity>         identities;

            for (int i = 0 ; i < package->faceRects.size() ; ++i)
            {
                QRect faceRect(std::round(package->image.width()  * package->faceRects[i].x()),
                               std::round(package->image.height() * package->faceRects[i].y()),
                               std::round(package->image.width()  * package->faceRects[i].width()),
                               std::round(package->image.height() * package->faceRects[i].height()));

                if (package->labelList[i] != -1)
                {
                    Identity identity = idProvider->identity(package->labelList[i]);
                    names << identity.attribute(QStringLiteral("name"));
                    identities << identity;
                    databaseFaces << FaceTagsIface(FaceTagsIface::Type::UnconfirmedName,
                                                   package->info.id(),
                                                   FaceTags::unconfirmedPersonTagId(),
                                                   TagRegion(faceRect));
                }
                else
                {
                    identities << Identity();
                    databaseFaces << FaceTagsIface(FaceTagsIface::Type::UnknownName,
                                                   package->info.id(),
                                                   FaceTags::unknownPersonTagId(),
                                                   TagRegion(faceRect));
                }
            }

            // store the thumbnails

            if (!package->image.isNull())
            {
                utils.storeThumbnails(thumbnailLoadThread, package->info.filePath(),
                                      databaseFaces, package->image);
            }

            // write the new face rects to the database

            utils.writeUnconfirmedResults(package->info.id(),
                                          package->faceRects,
                                          identities,
                                          package->image.originalSize());
        }

        QString albumName = CollectionManager::instance()->albumRootLabel(package->info.albumRootId());

        // send a notification that the image was processed

        notify(MLPipelineNotification::notifyProcessed,
               package->info.name(),
               albumName + package->info.relativePath(),
               names.join(QLatin1String(", ")),
               package->faceRects.size(),
               package->thumbnailIcon);

        // delete the package

        delete package;
    }

    /* =========================================================================================
     * End pipeline stage specific loop
     */

    MLPIPELINE_LOOP_END(MLPipelineStage::Writer, "FacePipelineDetectRecognize::writer");

    /* =========================================================================================
     * Pipeline stage specific cleanup
     *
     * Use the block from here to MLPIPELINE_STAGE_END to clean up any resources used by the stage.
     */

    MLPIPELINE_STAGE_END(MLPipelineStage::Writer, MLPipelineStage::None);
}

void FacePipelineDetectRecognize::addMoreWorkers()
{
    /* =========================================================================================
     * Use the performanceProfile metrics to find the slowest stages
     * and add more workers to those stages.
     *
     * For the Face detection and recognition pipeline, the loader is the
     * slowest stage so add 3 more loaders, 2 more extractors, and 1 more classifier.
     */

    addWorker(Loader);
    addWorker(Loader);
    addWorker(Loader);
    addWorker(Extractor);
    addWorker(Extractor);
    addWorker(Classifier);
}

} // namespace Digikam

#include "moc_facepipelinedetectrecognize.cpp"
