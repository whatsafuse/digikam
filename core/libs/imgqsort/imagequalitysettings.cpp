/* ============================================================
 *
 * This file is a part of digiKam project
 * https://www.digikam.org
 *
 * Date        : 2013-08-19
 * Description : Image quality Settings Container.
 *
 * SPDX-FileCopyrightText: 2013-2025 by Gilles Caulier <caulier dot gilles at gmail dot com>
 * SPDX-FileCopyrightText: 2021-2022 by Phuoc Khanh Le <phuockhanhnk94 at gmail dot com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * ============================================================ */

#include "imagequalitysettings.h"

// KDE includes

#include <kconfiggroup.h>
#include <ksharedconfig.h>

namespace Digikam
{

ImageQualitySettings::ImageQualitySettings(const ImageQualitySettings& other)
    : scanMode          (other.scanMode),
      wholeAlbums       (other.wholeAlbums),
      albums            (other.albums),
      useFullCpu        (other.useFullCpu),
      detectBlur        (other.detectBlur),
      detectNoise       (other.detectNoise),
      detectCompression (other.detectCompression),
      detectExposure    (other.detectExposure),
      detectAesthetic   (other.detectAesthetic),
      lowQRejected      (other.lowQRejected),
      mediumQPending    (other.mediumQPending),
      highQAccepted     (other.highQAccepted),
      rejectedThreshold (other.rejectedThreshold),
      pendingThreshold  (other.pendingThreshold),
      acceptedThreshold (other.acceptedThreshold),
      blurWeight        (other.blurWeight),
      noiseWeight       (other.noiseWeight),
      compressionWeight (other.compressionWeight),
      exposureWeight    (other.exposureWeight)
{
}

ImageQualitySettings& ImageQualitySettings::operator=(const ImageQualitySettings& other)
{
    scanMode           = other.scanMode;
    wholeAlbums        = other.wholeAlbums;
    albums             = other.albums;
    useFullCpu         = other.useFullCpu;
    detectBlur         = other.detectBlur;
    detectNoise        = other.detectNoise;
    detectCompression  = other.detectCompression;
    detectExposure     = other.detectExposure;
    detectAesthetic    = other.detectAesthetic;
    lowQRejected       = other.lowQRejected;
    mediumQPending     = other.mediumQPending;
    highQAccepted      = other.highQAccepted;
    rejectedThreshold  = other.rejectedThreshold;
    pendingThreshold   = other.pendingThreshold;
    acceptedThreshold  = other.acceptedThreshold;
    blurWeight         = other.blurWeight;
    noiseWeight        = other.noiseWeight;
    compressionWeight  = other.compressionWeight;
    exposureWeight     = other.exposureWeight;

    return *this;
}

void ImageQualitySettings::readFromConfig()
{
    KSharedConfig::Ptr config = KSharedConfig::openConfig();
    KConfigGroup group        = config->group(QLatin1String("Image Quality Settings"));
    readFromConfig(group);
}

void ImageQualitySettings::readFromConfig(const KConfigGroup& group)
{
    scanMode                  = (ScanMode)group.readEntry("Scan Mode", (int)ScanMode::AllItems);

    // NOTE: Album settings are handled by AlbumSelector widget.

    useFullCpu                = group.readEntry("Use Full CPU",        false);
    detectBlur                = group.readEntry("Detect Blur",         true);
    detectNoise               = group.readEntry("Detect Noise",        true);
    detectCompression         = group.readEntry("Detect Compression",  true);
    detectExposure            = group.readEntry("Detect Exposure",     true);
    detectAesthetic           = group.readEntry("Detect Aesthetic",    true);
    lowQRejected              = group.readEntry("LowQ Rejected",       true);
    mediumQPending            = group.readEntry("MediumQ Pending",     true);
    highQAccepted             = group.readEntry("HighQ Accepted",      true);
    rejectedThreshold         = group.readEntry("Rejected Threshold",  10);
    pendingThreshold          = group.readEntry("Pending Threshold",   40);
    acceptedThreshold         = group.readEntry("Accepted Threshold",  60);
    blurWeight                = group.readEntry("Blur Weight",         100);
    noiseWeight               = group.readEntry("Noise Weight",        100);
    compressionWeight         = group.readEntry("Compression Weight",  100);
    exposureWeight            = group.readEntry("Exposure Weight",     100);
}

void ImageQualitySettings::writeToConfig()
{
    KSharedConfig::Ptr config = KSharedConfig::openConfig();
    KConfigGroup group        = config->group(QLatin1String("Image Quality Settings"));
    writeToConfig(group);
}

void ImageQualitySettings::writeToConfig(KConfigGroup& group)
{
    group.writeEntry("Scan Mode",           (int)scanMode);

    // NOTE: Album settings are handled by AlbumSelector widget.

    group.writeEntry("Use Full CPU",        useFullCpu);
    group.writeEntry("Detect Blur",         detectBlur);
    group.writeEntry("Detect Noise",        detectNoise);
    group.writeEntry("Detect Compression",  detectCompression);
    group.writeEntry("Detect Exposure",     detectExposure);
    group.writeEntry("Detect Aesthetic",    detectAesthetic);
    group.writeEntry("LowQ Rejected",       lowQRejected);
    group.writeEntry("MediumQ Pending",     mediumQPending);
    group.writeEntry("HighQ Accepted",      highQAccepted);
    group.writeEntry("Rejected Threshold",  rejectedThreshold);
    group.writeEntry("Pending Threshold",   pendingThreshold);
    group.writeEntry("Accepted Threshold",  acceptedThreshold);
    group.writeEntry("Blur Weight",         blurWeight);
    group.writeEntry("Noise Weight",        noiseWeight);
    group.writeEntry("Compression Weight",  compressionWeight);
    group.writeEntry("Exposure Weight",     exposureWeight);
}

QDebug operator<<(QDebug dbg, const ImageQualitySettings& s)
{
    dbg.nospace()                                                   << Qt::endl;
    dbg.nospace() << "Scan Mode          :" << s.scanMode           << Qt::endl;
    dbg.nospace() << "Whole Albums       :" << s.wholeAlbums        << Qt::endl;
    dbg.nospace() << "Albums             :" << s.albums             << Qt::endl;
    dbg.nospace() << "Use Full CPU       :" << s.useFullCpu         << Qt::endl;
    dbg.nospace() << "DetectBlur         :" << s.detectBlur         << Qt::endl;
    dbg.nospace() << "DetectNoise        :" << s.detectNoise        << Qt::endl;
    dbg.nospace() << "DetectCompression  :" << s.detectCompression  << Qt::endl;
    dbg.nospace() << "DetectExposure     :" << s.detectExposure     << Qt::endl;
    dbg.nospace() << "DetectAesthetic    :" << s.detectAesthetic    << Qt::endl;
    dbg.nospace() << "LowQRejected       :" << s.lowQRejected       << Qt::endl;
    dbg.nospace() << "MediumQPending     :" << s.mediumQPending     << Qt::endl;
    dbg.nospace() << "HighQAccepted      :" << s.highQAccepted      << Qt::endl;
    dbg.nospace() << "Rejected Threshold :" << s.rejectedThreshold  << Qt::endl;
    dbg.nospace() << "Pending Threshold  :" << s.pendingThreshold   << Qt::endl;
    dbg.nospace() << "Accepted Threshold :" << s.acceptedThreshold  << Qt::endl;
    dbg.nospace() << "Blur Weight        :" << s.blurWeight         << Qt::endl;
    dbg.nospace() << "Noise Weight       :" << s.noiseWeight        << Qt::endl;
    dbg.nospace() << "Compression Weight :" << s.compressionWeight  << Qt::endl;
    dbg.nospace() << "Exposure Weight    :" << s.exposureWeight     << Qt::endl;

    return dbg.space();
}

} // namespace Digikam

#include "moc_imagequalitysettings.cpp"
